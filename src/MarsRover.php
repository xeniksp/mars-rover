<?php

/**
 * Class MarsRover
 */
class MarsRover
{

    const DIRECTION_MAP = [
      'N' => [
        'L' => 'W',
        'R' => 'E',
        'move' => ['y' => 1],
      ],
      'S' => [
        'L' => 'E',
        'R' => 'W',
        'move' => ['y' => -1],
      ],
      'W' => [
        'L' => 'S',
        'R' => 'N',
        'move' => ['x' => -1],
      ],
      'E' => [
        'L' => 'N',
        'R' => 'S',
        'move' => ['x' => 1],
      ],
    ];


    private $mapSize;

    private $rovers;

    /**
     * MarsRover constructor.
     *
     * @param string $orders
     *
     * @throws \Exception
     */
    public function __construct(string $orders)
    {
        $orders = $this->setOrders($orders);
        $this->mapSize = $this->setMapSize(array_shift($orders));
        $this->rovers = array_chunk($orders, 2);
    }

    /**
     * @throws \Exception
     */
    public function getLastCoordinates()
    {
        foreach ($this->rovers as $roverKey => $rover) {
            $instructions = [];
            $roverPosition = [];
            $this->setPositionAndInstuctions(
              $roverKey,
              $rover,
              $instructions,
              $roverPosition
            );
            if (!empty($instructions) && !empty($roverPosition)) {
                $currentDirection = $roverPosition[2];
                $currentPosition = [
                  'x' => $roverPosition[0],
                  'y' => $roverPosition[1],
                ];
                $this->calculateLastCoordinates(
                  $currentDirection,
                  $currentPosition,
                  $instructions,
                  $roverKey
                );
                echo implode(' ', $currentPosition).' '.$currentDirection;
                echo "<br/>\n";
            }
        }
    }

    /**
     * @param string $mapSize
     *
     * @return array
     * @throws \Exception
     */
    private function setMapSize(string $mapSize)
    {
        $mapSizeArray = explode(' ', $mapSize);
        if (count($mapSizeArray) !== 2) {
            throw new Exception('Wrong map size');
        } else {
            $mapSize = [
              'x' => $mapSizeArray[0],
              'y' => $mapSizeArray[1],
            ];

            return $mapSize;
        }
    }

    /**
     * @param string $axis
     * @param int $axisValue
     * @param int $roverKey
     *
     * @return bool
     * @throws \Exception
     */
    private function isInsideMap(string $axis, int $axisValue, int $roverKey)
    {
        if ($this->mapSize[$axis] < $axisValue || $axisValue < 0) {
            throw new Exception('Rover '.$roverKey.' goes out of map');
        } else {
            return true;
        }
    }

    /**
     * @param string $orders
     *
     * @return array
     * @throws \Exception
     */
    private function setOrders(string $orders)
    {
        $orders = preg_split("/\r\n|\n|\r/", $orders);
        $ordersCount = count($orders);
        if ($ordersCount > 2 && $ordersCount % 2 === 1) {
            return $orders;
        } else {
            throw new Exception('Wrong orders input');
        }
    }


    /**
     * @param string $instructions
     * @param int $roverKey
     *
     * @return array
     * @throws \Exception
     */
    private function setInstructions(
      string $instructions,
      int $roverKey
    ) {
        if (preg_match('/^[L,M,R]*+$/', $instructions)) {
            return str_split($instructions);
        } else {
            throw new Exception(
              'Wrong movement instructions for rover '.$roverKey
            );
        }
    }

    /**
     * @param string $currentDirection
     * @param array $currentPosition
     * @param array $instructions
     * @param int $roverKey
     *
     * @throws \Exception
     */
    private function calculateLastCoordinates(
      string &$currentDirection,
      array &$currentPosition,
      array $instructions,
      int $roverKey
    ) {
        foreach ($instructions as $instruction) {
            if (in_array($instruction, ['L', 'R'])) {
                $currentDirection = self::DIRECTION_MAP[$currentDirection][$instruction];
            } else {
                $move = self::DIRECTION_MAP[$currentDirection]['move'];
                $axis = key($move);
                $axisValue = $currentPosition[$axis] + reset($move);
                if ($this->isInsideMap($axis, $axisValue, $roverKey + 1)) {
                    $currentPosition[$axis] = $axisValue;
                }
            }
        }
    }

    /**
     * @param int $roverKey
     * @param array $rover
     * @param array $instructions
     * @param array $roverPosition
     *
     * @throws \Exception
     */
    private function setPositionAndInstuctions(
      int $roverKey,
      array $rover,
      array &$instructions,
      array &$roverPosition
    ) {
        foreach ($rover as $roverOrderKey => $roverOrder) {
            if ($roverOrderKey === 0) {
                $roverPosition = explode(' ', $roverOrder);
            } else {
                $instructions = $this->setInstructions(
                  $roverOrder,
                  $roverKey + 1
                );
            }
        }
    }
}
