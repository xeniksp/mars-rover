<?php

require 'src/MarsRover.php';

$orders = '5 5
1 2 N
LMLMLMLMM
3 3 E
MMRMMRMRRM';

try {
    $marsRover = new MarsRover($orders);
    $marsRover->getLastCoordinates();
} catch (Exception $e) {
    var_dump($e->getMessage());
}